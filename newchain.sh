#!/bin/sh

doublet=$1
dotted=$(echo $1 | sed -e 's/-/./g')

cp -r jp.pizzafactory.crosschains.arm.elf-feature jp.pizzafactory.crosschains.${dotted}-feature
cp -r jp.pizzafactory.crosschains.arm.elf jp.pizzafactory.crosschains.${dotted}

for i in jp.pizzafactory.crosschains.${dotted}*/*; do
  if [ -f $i ]; then
    echo $i
    sed -i -e "s/arm-elf/${doublet}/g" $i
  fi
done

for i in jp.pizzafactory.crosschains.${dotted}*/*; do
  if [ -f $i ]; then
    echo $i
    sed -i -e "s/arm\\.elf/${dotted}/g" $i
  fi
done

